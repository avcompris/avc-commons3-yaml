# About avc-commons3-yaml

New flavor of YAML utilities
that were in
[avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)


This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-commons3-yaml/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons3-yaml/)

