package net.avcompris.commons3.yaml;

import javax.annotation.Nullable;

public interface Logger {

	void debug(@Nullable String message);
}
