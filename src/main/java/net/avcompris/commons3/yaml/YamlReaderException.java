package net.avcompris.commons3.yaml;

import javax.annotation.Nullable;

public class YamlReaderException extends RuntimeException {

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = -2263882857057481417L;

	private final int lineNumber;
	private final int columnNumber;

	public final int getLineNumber() {

		return lineNumber;
	}

	public final int getColumnNumber() {

		return columnNumber;
	}

	public YamlReaderException(final int lineNumber, final int columnNumber, @Nullable final String message) {

		super(lineNumber + ":" + columnNumber + ": " + message);

		this.lineNumber = lineNumber;
		this.columnNumber = columnNumber;
	}
}
