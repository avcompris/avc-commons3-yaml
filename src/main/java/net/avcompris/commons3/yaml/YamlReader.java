package net.avcompris.commons3.yaml;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.io.IOException;
import java.io.Reader;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;

final class YamlReader {

	private final Reader reader;

	private int currentLineNumber = 1;
	private int currentColumnNumber = 1;

	private int currentTokenLineNumber = 1;
	private int currentTokenColumnNumber = 1;

	public int getCurrentLineNumber() {

		return currentLineNumber;
	}

	public int getCurrentColumnNumber() {

		return currentColumnNumber;
	}

	public int getCurrentTokenLineNumber() {

		return currentTokenLineNumber;
	}

	public int getCurrentTokenColumnNumber() {

		return currentTokenColumnNumber;
	}

	public YamlReader(
		final Reader reader
	) {

		this.reader = checkNotNull(reader, "reader");
	}

	private final List<Character> nextC = newArrayList();

	private int read() throws IOException {

		if (!nextC.isEmpty()) {

			final int c = nextC.get(0);

			nextC.remove(0);

			if (c == -1) {

				isEof = true;
			}

			return c;

		} else {

			final int n = reader.read();

			if (n == -1) {

				isEof = true;
			}

			return n;
		}
	}

	private Token nextToken = null;

	public static final class Token {

		public static final Token OPEN_CURLY_BRACKET = new Token("{", false);
		public static final Token CLOSE_CURLY_BRACKET = new Token("}", false);
		public static final Token OPEN_SQUARE_BRACKET = new Token("[", false);
		public static final Token CLOSE_SQUARE_BRACKET = new Token("]", false);
		public static final Token HYPHEN = new Token("-", false);
		public static final Token COLON = new Token(":", false);
		public static final Token COMMA = new Token(",", false);
		public static final Token LINEBREAK = new Token("\n", false);

		public static final Token get(
			final char c
		) {

			if (c == ':') {

				return COLON;

			} else if (c == ',') {

				return COMMA;

			} else if (c == '}') {

				return CLOSE_CURLY_BRACKET;

			} else if (c == ']') {

				return CLOSE_SQUARE_BRACKET;

			} else {

				throw new IllegalArgumentException("c: " + c + " (" + ((int) c) + ")");
			}
		}

		public final String s;
		public final boolean isQuoted;

		private Token(
			final String s,
			final boolean isQuoted
		) {

			this.s = checkNotNull(s, "s");
			this.isQuoted = isQuoted;
		}

		@Override
		public String toString() {

			if (this == OPEN_CURLY_BRACKET) {

				return "OPEN_CURLY_BRACKET[{]";

			} else if (this == CLOSE_CURLY_BRACKET) {

				return "CLOSE_CURLY_BRACKET[}]";

			} else if (this == OPEN_SQUARE_BRACKET) {

				return "OPEN_SQUARE_BRACKET<[>";

			} else if (this == CLOSE_SQUARE_BRACKET) {

				return "CLOSE_SQUARE_BRACKET<]>";

			} else if (this == COLON) {

				return "COLON[:]";

			} else if (this == COMMA) {

				return "COMMA[,]";

			} else if (this == HYPHEN) {

				return "HYPHEN[-]";

			} else if (this == LINEBREAK) {

				return "LINEBREAK";

			} else {

				return s;
			}
		}

		@Override
		public boolean equals(
			@Nullable final Object o
		) {

			if (o == null || !(o instanceof Token)) {

				return false;
			}

			return s.equals(((Token) o).s);
		}

		@Override
		public int hashCode() {

			return s.hashCode();
		}
	}

	@Nullable
	public Token readNextToken(
		final boolean breakAtComma
	) throws IOException {

		if (nextToken == null) {

			return readToken(breakAtComma);

		} else {

			final Token token = nextToken;

			nextToken = null;

			return token;
		}
	}

	private boolean isEof = false;

	private int currentIndent = -1;

	public int readIndent() throws IOException {

		if (!isEof) {

			if (currentColumnNumber != 1) {

				return currentIndent;
			}

			// throw new IllegalStateException("currentColumnNumber: " + currentColumnNumber
			// + " != 1");
		}

		int indent = 0;

		loop: while (true) {

			final int n = read();

			if (n == -1) {

				currentIndent = -1;

				return currentIndent;
			}

			final char c = (char) n;

			if (c == '\t') {

				throw new YamlReaderException( //
						currentLineNumber, //
						currentColumnNumber, //
						"Illegal char: " + c + " (" + ((int) c) + ")");
			}

			++currentColumnNumber;

			if (c == ' ') {

				++indent;

				continue;

			} else if (c == '\n') {

				++currentLineNumber;
				currentColumnNumber = 1;

				indent = 0;

				continue;

			} else if (c == '\r') {

				throw new NotImplementedException("");

			} else if (c == '#') {

				while (true) {

					final int n2 = read();

					if (n2 == -1) {

						return -1;
					}

					final char c2 = (char) n2;

					if (c2 == '\n') {

						++currentLineNumber;
						currentColumnNumber = 1;

						indent = 0;

						continue loop;

					} else if (c == '\r') {

						throw new NotImplementedException("");
					}
				}

			} else {

				nextC.add(c);

				currentIndent = indent;

				return currentIndent;
			}
		}
	}

	private StringBuilder reservedToken = null;

	private void reserveToken(
		final char c
	) {

		if (reservedToken == null) {

			reservedToken = new StringBuilder();
		}

		reservedToken.append(c);
	}

	@Nullable
	private Token readToken(
		final boolean breakAtComma
	) throws IOException {

		reservedToken = null;

		final StringBuilder sb = new StringBuilder();

		currentTokenLineNumber = currentLineNumber;
		currentTokenColumnNumber = currentColumnNumber;

		while (true) {

			final int n = read();

			if (n == -1) {

				if (sb.length() == 0) {

					return null;

				} else {

					return new Token(sb.toString(), false);
				}
			}

			final char c = (char) n;

			if (c == '\t') {

				throw new YamlReaderException( //
						currentLineNumber, //
						currentColumnNumber, //
						"Illegal char: " + c + " (" + ((int) c) + ")");
			}

			++currentColumnNumber;

			if (c == '\n') {

				++currentLineNumber;
				currentColumnNumber = 1;

				if (sb.length() == 0) {

					currentTokenColumnNumber = currentColumnNumber;

					return Token.LINEBREAK;

				} else {

					nextToken = Token.LINEBREAK;

					break;
				}

			} else if (c == '\r') {

				throw new NotImplementedException("");

			} else if (c == '{') {

				if (sb.length() == 0) {

					return Token.OPEN_CURLY_BRACKET;

				} else {

					sb.append(c);
				}

			} else if (c == '-') {

				if (sb.length() == 0) {

					return Token.HYPHEN;
				}

				if (reservedToken != null) {

					sb.append(reservedToken.toString());

					reservedToken = null;
				}

				sb.append(c);

			} else if (c == '[') {

				if (sb.length() == 0) {

					return Token.OPEN_SQUARE_BRACKET;

					// } else {
					//
					// throw new YamlReaderException( //
					// currentLineNumber, //
					// currentColumnNumber, //
					// "Illegal opening bracket \"[\" after: \"" + sb + "\"");
				}

				sb.append(c);

			} else if (c == '#' && sb.isEmpty()) {

				while (true) {

					final int n2 = read();

					if (n2 == -1) {

						return null;
					}

					final char c2 = (char) n2;

					if (c2 == '\n') {

						++currentLineNumber;
						currentColumnNumber = 1;

						if (sb.length() == 0) {

							currentTokenColumnNumber = currentColumnNumber;

							return Token.LINEBREAK;

							// continue loop;

						} else {

							return new Token(sb.toString(), false);
						}

					} else if (c2 == '\r') {

						throw new NotImplementedException("");
					}
				}

			} else if (c == ':') {

				if (sb.length() == 0) {

					return Token.get(c);
				}

				final int n2 = read();

				if (n2 == -1) {
					throw new NotImplementedException("n2 == -1");
				}

				final char nextC = (char) n2;

				if (nextC == ' ' || nextC == '\n' || nextC == '\r' || nextC == '{') {

					this.nextC.add(c);
					this.nextC.add(nextC);

					return new Token(sb.toString(), false);

				} else {

					// this.nextC = null;

					sb.append(c).append(nextC);
				}

			} else if (c == ',' && breakAtComma) {

				if (sb.length() == 0) {

					return Token.get(c);

				} else {

					nextC.add(c);

					return new Token(sb.toString(), false);

					// if (reservedToken != null) {

					// sb.append(reservedToken.toString());

					// reservedToken = null;
					// }

					// sb.append(c);
				}

			} else if (c == '}' || c == ']') {

				if (sb.length() == 0) {

					return Token.get(c);

				} else if (previousIs(' ')) {

					nextC.add(c);

					return new Token(sb.toString(), false);

				} else {

					if (reservedToken != null) {

						sb.append(reservedToken.toString());

						reservedToken = null;
					}

					sb.append(c);
				}

			} else if (c == ' ') {

				if (sb.length() == 0) {

					currentTokenColumnNumber = currentColumnNumber;

					continue;

				} else {

					reserveToken(' ');

					continue;
				}

			} else if (c == '\'' && sb.length() == 0) {

				final int currentLineNumber0 = currentLineNumber;
				final int currentColumnNumber0 = currentColumnNumber;

				while (true) {

					final int n2 = read();

					if (n2 == -1) {

						throw new YamlReaderException( //
								currentLineNumber0, //
								currentColumnNumber0, //
								"Unterminated quoted string");
					}

					++currentColumnNumber;

					final char c2 = (char) n2;

					if (c2 == '\'') {

						boolean quoted = true;

						final StringBuilder trailing = new StringBuilder();

						while (true) {

							final int n3 = read();

							if (n3 == ' ') {

								if (!isBlank(trailing)) {

									if (quoted) {
										sb.insert(0, '\'');
										sb.append('\'');
										quoted = false;
									}

									sb.append(trailing);

									trailing.setLength(0);
								}
							}

							if (n3 == -1 || n3 == '\n' || n3 == '\r' || n3 == '}') {

								if (n3 != -1) {

									nextC.add((char) n3);
								}

								if (!isBlank(trailing)) {

									if (quoted) {
										sb.insert(0, '\'');
										sb.append('\'');
										quoted = false;
									}

									sb.append(trailing);
								}

								break;
							}

							trailing.append((char) n3);
						}

						return new Token(sb.toString(), quoted);

					} else if (c2 == '\\') {

						final int n3 = read();

						if (n3 == -1) {

							throw new YamlReaderException( //
									currentLineNumber0, //
									currentColumnNumber0, //
									"Unterminated quoted string");
						}

						final char c3 = (char) n3;

						if (c3 == '\'') {

							sb.append('\'');

						} else if (c3 == '\\') {

							sb.append('\\');

						} else {

							throw new YamlReaderException( //
									currentLineNumber, //
									currentColumnNumber, //
									"Illegal escape code: \"" + c2 + c3 + "\"");
						}

						++currentColumnNumber;

						continue;
					}

					sb.append(c2);
				}

			} else if (c == '"' && sb.length() == 0) {

				final int currentLineNumber0 = currentLineNumber;
				final int currentColumnNumber0 = currentColumnNumber;

				while (true) {

					final int n2 = read();

					if (n2 == -1) {

						throw new YamlReaderException( //
								currentLineNumber0, //
								currentColumnNumber0, //
								"Unterminated quoted string");
					}

					++currentColumnNumber;

					final char c2 = (char) n2;

					if (c2 == '"') {

						return new Token(sb.toString(), true);

					} else if (c2 == '\\') {

						final int n3 = read();

						if (n3 == -1) {

							throw new YamlReaderException( //
									currentLineNumber0, //
									currentColumnNumber0, //
									"Unterminated quoted string");
						}

						final char c3 = (char) n3;

						if (c3 == '"') {

							sb.append('"');

						} else if (c3 == '\\') {

							sb.append('\\');

						} else {

							throw new YamlReaderException( //
									currentLineNumber, //
									currentColumnNumber, //
									"Illegal escape code: \"" + c2 + c3 + "\"");
						}

						++currentColumnNumber;

						continue;
					}

					sb.append(c2);
				}

			} else {

				if (reservedToken != null) {

					sb.append(reservedToken.toString());

					reservedToken = null;
				}

				sb.append(c);
			}
		}

		return new Token(sb.toString(), false);
	}

	private boolean previousIs(
		final char c
	) {

		if (reservedToken != null && !reservedToken.isEmpty()) {

			final int l = reservedToken.length();

			return reservedToken.substring(l - 1, l).charAt(0) == c;
		}

		return nextC != null && !nextC.isEmpty() && nextC.get(nextC.size() - 1) == c;
	}

	@Nullable
	public Token peekNextToken(
		final boolean inArray
	) throws IOException {

		if (nextToken != null) {

			return nextToken;

		} else {

			nextToken = readToken(inArray);

			return nextToken;
		}
	}
}
