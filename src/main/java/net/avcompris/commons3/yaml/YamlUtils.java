package net.avcompris.commons3.yaml;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import org.apache.commons.lang3.NotImplementedException;
// import org.yaml.snakeyaml.constructor.SafeConstructor;

public abstract class YamlUtils {

	public static Yaml loadYaml(final InputStream is) throws IOException {

		checkNotNull(is, "is");

		// final org.yaml.snakeyaml.Yaml snakeYaml = new org.yaml.snakeyaml.Yaml(new SafeConstructor());

		// @SuppressWarnings("unchecked")
		// final Map<Object, Object> m = (Map<Object, Object>) snakeYaml.load(is);

		final Map<Object, Object> map;

		try (BufferedInputStream bis = new BufferedInputStream(is)) {

			try (Reader reader = new InputStreamReader(bis, UTF_8)) {

				map = new YamlLoader().load(reader);
			}
		}

		return new YamlImpl(map);
	}

	public static Yaml loadYaml(final File yamlFile) throws IOException {

		checkNotNull(yamlFile, "yamlFile");

		try (InputStream is = new FileInputStream(yamlFile)) {

			return loadYaml(is);
		}
	}

	public static Yaml loadYaml(final String yamlContent) throws IOException {

		checkNotNull(yamlContent, "yamlContent");

		// final org.yaml.snakeyaml.Yaml snakeYaml = new org.yaml.snakeyaml.Yaml(new SafeConstructor());

		// @SuppressWarnings("unchecked")
		// final Map<Object, Object> map = (Map<Object, Object>) snakeYaml.load(yamlContent);

		final Map<Object, Object> map;

		try (Reader reader = new StringReader(yamlContent)) {

			map = new YamlLoader().load(reader);
		}

		return new YamlImpl(map);
	}

	private static final class YamlImpl implements Yaml {

		private static final Yaml NULL = new YamlImpl();

		@Nullable
		private final Map<Object, Object> map;

		@Nullable
		private final List<Object> list;

		@Nullable
		private final String s;

		@Nullable
		private final Integer n;

		@Nullable
		private final Long l;

		@Nullable
		private final Boolean b;

		@Nullable
		private final Date date;

		YamlImpl(@Nullable final Map<Object, Object> map) {

			this.map = map;
			this.list = null;
			this.s = null;
			this.n = null;
			this.l = null;
			this.b = null;
			this.date = null;
		}

		YamlImpl(final List<Object> list) {

			this.map = null;
			this.list = checkNotNull(list, "list");
			this.s = null;
			this.n = null;
			this.l = null;
			this.b = null;
			this.date = null;
		}

		YamlImpl(final String s) {

			this.map = null;
			this.list = null;
			this.s = checkNotNull(s, "s");
			this.n = null;
			this.l = null;
			this.b = null;
			this.date = null;
		}

		YamlImpl(final int n) {

			this.map = null;
			this.list = null;
			this.s = null;
			this.n = n;
			this.l = (long) n;
			this.b = null;
			this.date = null;
		}

		YamlImpl(final long l) {

			this.map = null;
			this.list = null;
			this.s = null;
			this.n = null;
			this.l = l;
			this.b = null;
			this.date = null;
		}

		YamlImpl(final boolean b) {

			this.map = null;
			this.list = null;
			this.s = null;
			this.n = null;
			this.l = null;
			this.b = b;
			this.date = null;
		}

		YamlImpl(final Date date) {

			this.map = null;
			this.list = null;
			this.s = null;
			this.n = null;
			this.l = null;
			this.b = null;
			this.date = checkNotNull(date, "date");
		}

		private YamlImpl() {

			this.map = null;
			this.list = null;
			this.s = null;
			this.n = null;
			this.l = null;
			this.b = null;
			this.date = null;
		}

		@Override
		public Iterable<Object> keys() {

			return map.keySet();
		}

		@Override
		public boolean has(final Object key) {

			checkNotNull(key, "key");

			return map != null && map.containsKey(key);
		}

		@Override
		public Yaml get(final Object key) {

			checkNotNull(key, "key");

			checkState(map != null, "Internal map should not be null");

			checkArgument(map.containsKey(key), "key: %s", key);

			final Object value = map.get(key);

			if (value == null) {

				return YamlImpl.NULL;

			} else {

				return toYaml(value);
			}
		}

		private static Yaml toYaml(final Object value) {

			checkNotNull(value, "value");

			if (value instanceof Map) {

				@SuppressWarnings("unchecked")
				final Map<Object, Object> map = (Map<Object, Object>) value;

				return new YamlImpl(map);

			} else if (value instanceof List) {

				@SuppressWarnings("unchecked")
				final List<Object> list = (List<Object>) value;

				return new YamlImpl(list);

			} else if (value instanceof String) {

				final String s = (String) value;

				return new YamlImpl(s);

			} else if (value instanceof Integer) {

				final int n = (Integer) value;

				return new YamlImpl(n);

			} else if (value instanceof Long) {

				final long l = (Long) value;

				return new YamlImpl(l);

			} else if (value instanceof Boolean) {

				final boolean b = (Boolean) value;

				return new YamlImpl(b);

			} else if (value instanceof Date) {

				final Date date = (Date) value;

				return new YamlImpl(date);
			}

			throw new NotImplementedException("value.class: " + value.getClass());
		}

		@Override
		public Iterable<String> keysAsStrings() {

			return map.keySet().stream() //
					.map(key -> key.toString()) //
					.sorted() //
					.collect(Collectors.toList());
		}

		@Override
		public boolean isArray() {

			return list != null;
		}

		@Override
		public boolean isMap() {

			return map != null;
		}

		@Override
		public boolean isString() {

			return s != null;
		}

		@Override
		public boolean isInt() {

			return n != null;
		}

		@Override
		public boolean isBoolean() {

			return b != null;
		}

		@Override
		public Date asDate() {

			checkState(date != null, "Internal date should not be null");

			return date;
		}

		@Override
		public String asString() {

			if (date != null) {

				throw new NotImplementedException("date: " + date);

			} else if (n != null) {

				return Integer.toString(n);

			} else if (l != null) {

				return Long.toString(l);

			} else if (b != null) {

				return Boolean.toString(b);
			}

			checkState(s != null, "Internal string should not be null");

			return s;
		}

		@Override
		public int asInt() {

			if (date != null) {

				throw new NotImplementedException("date: " + date);
			}

			checkState(n != null, "Internal int should not be null");

			return n;
		}

		@Override
		public long asLong() {

			if (date != null) {

				throw new NotImplementedException("date: " + date);

			} else if (l != null) {

				return l;

			} else if (n != null) {

				return n;
			}

			throw new IllegalStateException("Internal long should not be null");
		}

		@Override
		public boolean asBoolean() {

			if (date != null) {
				throw new NotImplementedException("date: " + date);
			}

			if (b != null) {

				return b;
			}

			if (s != null) {

				if ("yes".equals(s)) {

					return true;

				} else if ("no".equals(s)) {

					return false;

				} else {

					throw new NotImplementedException("Cannot parse string to boolean: " + s);
				}
			}

			throw new IllegalStateException("Internal boolean should not be null");
		}

		@Override
		public Iterable<Yaml> items() {

			return list.stream() //
					.map(item -> toYaml(item)) //
					.collect(Collectors.toList());
		}

		@Override
		public String toString() {

			final StringBuilder sb = new StringBuilder("{");

			if (s != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("s: ").append(s);
			}
			if (n != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("n: ").append(n);
			}
			if (l != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("l: ").append(l);
			}
			if (b != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("b: ").append(b);
			}
			if (date != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("date: ").append(date);
			}
			if (list != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("list: ").append(list);
			}
			if (map != null) {
				sb.append(sb.length() == 1 ? "" : ", ").append("map: ").append(map);
			}

			return sb.append("}").toString();
		}
	}
}
