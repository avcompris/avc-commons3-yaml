package net.avcompris.commons3.yaml;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

import java.util.Map;

import javax.annotation.Nullable;

public abstract class LoggerFactory {

	private static final Map<Class<?>, Logger> loggers = newHashMap();

	public static final boolean DEBUG = false;

	public static Logger getLogger(final Class<?> clazz) {

		checkNotNull(clazz, "clazz");

		final Logger cached = loggers.get(clazz);

		if (cached != null) {

			return cached;
		}

		final Logger logger = new Logger() {

			@Override
			public void debug(@Nullable final String message) {

				if (DEBUG) {

					System.out.println("DEBUG: " + message);
				}
			}
		};

		loggers.put(clazz, logger);

		return logger;
	}
}
