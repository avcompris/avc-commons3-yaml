package net.avcompris.commons3.yaml;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static net.avcompris.commons3.yaml.YamlReader.Token.CLOSE_CURLY_BRACKET;
import static net.avcompris.commons3.yaml.YamlReader.Token.CLOSE_SQUARE_BRACKET;
import static net.avcompris.commons3.yaml.YamlReader.Token.COLON;
import static net.avcompris.commons3.yaml.YamlReader.Token.COMMA;
import static net.avcompris.commons3.yaml.YamlReader.Token.HYPHEN;
import static net.avcompris.commons3.yaml.YamlReader.Token.LINEBREAK;
import static net.avcompris.commons3.yaml.YamlReader.Token.OPEN_CURLY_BRACKET;
import static net.avcompris.commons3.yaml.YamlReader.Token.OPEN_SQUARE_BRACKET;

import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.NotImplementedException;

import net.avcompris.commons3.yaml.YamlReader.Token;

final class YamlLoader {

	private static final Logger logger = LoggerFactory.getLogger(YamlLoader.class);

	public Map<Object, Object> load(
		final Reader reader
	) throws IOException {

		checkNotNull(reader, "reader");

		final Map<Object, Object> map = newHashMap();

		final YamlReader yamlReader = new YamlReader(reader);

		while (true) {

			final Token token0 = yamlReader.readNextToken(false);

			logger.debug("main, token0: " + token0);

			if (token0 == null) {

				break;
			}

			if (token0 == OPEN_CURLY_BRACKET) {

				final Map<Object, Object> map2 = readMapWithClosingBracket(yamlReader);

				final Token token1 = yamlReader.readNextToken(false);

				if (token1 == null) {

					map.putAll(map2);

				} else if (token1 == COLON) {

					// yamlReader.readNextToken();

					final Object value = readValue(yamlReader);

					checkState(!map.containsKey(map2), "Duplicate key: " + map2);

					map.put(map2, value);

				} else {

					throw new YamlReaderException( //
							yamlReader.getCurrentLineNumber(), //
							yamlReader.getCurrentLineNumber(), //
							"Illegal token: \"" + token1 + "\"");
				}

			} else if (token0 == LINEBREAK) {

				continue;

			} else if (token0 == HYPHEN) {

				throw new NotImplementedException("List at 0-level.");

			} else {

				final Object key = toObject(token0);

				final Token token1 = yamlReader.readNextToken(false);

				logger.debug("main, token1: " + token1);

				if (token1 != COLON) {

					throw new YamlKeyException(yamlReader.getCurrentLineNumber(), //
							yamlReader.getCurrentColumnNumber(), //
							"Illegal key: \"" + token0 + "+" + token1 + "\"");
				}

				final Object value = readValue(yamlReader);

				checkState(!map.containsKey(key), "Duplicate key: " + key);

				map.put(key, value);
			}
		}

		return map;
	}

	private static Object readValue(
		final YamlReader yamlReader
	) throws IOException {

		logger.debug("readValue()...");

		final Token token0 = yamlReader.readNextToken(false);

		logger.debug("readValue(), token: " + token0);

		if (token0 == OPEN_CURLY_BRACKET) {

			return readMapWithClosingBracket(yamlReader);

		} else if (token0 == OPEN_SQUARE_BRACKET) {

			return readArrayWithClosingBracket(yamlReader);

		} else if (token0 == LINEBREAK) {

			final int indent = yamlReader.readIndent();

			return readValueWithIndent(yamlReader, indent);

		} else {

			final Object value0 = toObject(token0);

			if (value0 instanceof String || value0 instanceof Integer) {

				final StringBuilder sb = new StringBuilder(value0.toString());

				while (true) {

					final Token token1 = yamlReader.readNextToken(false);

					if (token1 == null || token1 == LINEBREAK) {

						break;
					}

					final Object value1 = toObject(token1);

					if (value1 instanceof String) {

						sb.append(value1.toString());

					} else {

						throw new NotImplementedException("value1: " + value1);
					}
				}

				return sb.toString();

			} else {

				return value0;
			}
		}
	}

	private static Object toObject(
		final Token token
	) {

		checkNotNull(token, "token");

		if (token.isQuoted) {

			return token.s; // String

		} else {

			if ("true".contentEquals(token.s)) {

				return Boolean.TRUE; // Boolean

			} else if ("false".contentEquals(token.s)) {

				return Boolean.FALSE; // Boolean
			}
			try {

				return Integer.parseInt(token.s); // Integer

			} catch (final NumberFormatException parseIntE) {

				try {

					return Long.parseLong(token.s); // Long

				} catch (final NumberFormatException parseLongE) {

					return token.s; // String
				}
			}
		}
	}

	private static Map<Object, Object> readMapWithClosingBracket(
		final YamlReader yamlReader
	) throws IOException {

		final Map<Object, Object> map = newHashMap();

		while (true) {

			final Token token0 = yamlReader.readNextToken(false);

			if (token0 != null) {

				if (token0 == CLOSE_CURLY_BRACKET) {

					return map;
				}

				final Object key = toObject(token0);

				final Token token1 = yamlReader.readNextToken(false);

				if (token1 != COLON) {

					throw new YamlKeyException( //
							yamlReader.getCurrentTokenLineNumber(), //
							yamlReader.getCurrentTokenColumnNumber(), //
							"Illegal key: " + token0 + "+" + token1);
				}

				final Token token2 = yamlReader.readNextToken(true);

				final Object value = toObject(token2);

				checkState(!map.containsKey(key), "Duplicate key: " + key);

				map.put(key, value);

				final Token token3 = yamlReader.readNextToken(true);

				if (token3 == CLOSE_CURLY_BRACKET || token3 == null) {

					break;

				} else if (token3 == COMMA) {

					continue;

				} else {

					throw new NotImplementedException("token3: " + token3);
				}
			}
		}

		return map;
	}

	private static List<Object> readArrayWithClosingBracket(
		final YamlReader yamlReader
	) throws IOException {

		logger.debug("readArrayWithClosingBracket()...");

		final List<Object> list = newArrayList();

		while (true) {

			final Token token0 = yamlReader.readNextToken(true);

			if (token0 != null) {

				if (token0 == CLOSE_SQUARE_BRACKET) {

					return list;
				}

				if (token0 == COMMA) {

					throw new YamlReaderException( //
							yamlReader.getCurrentTokenLineNumber(), //
							yamlReader.getCurrentTokenColumnNumber(), //
							"Illegal token: " + token0);
				}

				final Object item = toObject(token0);

				list.add(item);

				final Token token1 = yamlReader.readNextToken(true);

				if (token1 == CLOSE_SQUARE_BRACKET) {

					break;
				}

				if (token1 != COMMA) {

					throw new YamlKeyException( //
							yamlReader.getCurrentTokenLineNumber(), //
							yamlReader.getCurrentTokenColumnNumber(), //
							"Illegal token: " + token1);
				}
			}
		}

		return list;
	}

	private static Object readValueWithIndent(
		final YamlReader yamlReader,
		final int indent
	) throws IOException {

		logger.debug("readValueWithIndent(" + indent + ")...");

		final Map<Object, Object> map = newHashMap();

		while (true) {

			if (yamlReader.readIndent() < indent) {

				break;
			}

			final Token token0 = yamlReader.readNextToken(false);

			logger.debug("  token0: " + token0);

			if (token0 == LINEBREAK) {

				continue;
			}

			if (token0 == null) {

				break;
			}

			if (token0 == HYPHEN) {

				final Object array = readArrayWithIndent(yamlReader, indent);

				return array;
			}

			// if (token0 == CLOSE_BRACKET) {
			//
			// return map;
			// }

			final Token token1 = yamlReader.readNextToken(false);

			logger.debug("  token1: " + token1);

			if (token1 == null || token1 == LINEBREAK) {

				final int indent2 = yamlReader.readIndent();

				if (indent2 < indent) {

					final Object value = toObject(token0);

					return value;
				}
			}

			final Object key = toObject(token0);

			if (token1 != COLON) {

				throw new YamlKeyException( //
						yamlReader.getCurrentTokenLineNumber(), //
						yamlReader.getCurrentTokenColumnNumber(), //
						"Illegal key: " + token0 + "+" + token1);
			}

			final Token token2 = yamlReader.readNextToken(false);

			if (token2 == LINEBREAK) {

				final int indent3 = yamlReader.readIndent();

				if (indent3 <= indent) {

					throw new YamlReaderException( //
							yamlReader.getCurrentLineNumber(), //
							yamlReader.getCurrentTokenColumnNumber(), //
							"Unterminated colon");

				} else {

					final Object value = readValueWithIndent(yamlReader, indent3);

					checkState(!map.containsKey(key), "Duplicate key: " + key);

					map.put(key, value);

					final int indent4 = yamlReader.readIndent();

					if (indent4 < indent) {

						break;
					}

					continue;
				}

			} else if (token2 == OPEN_SQUARE_BRACKET) {

				final Object value2 = readArrayWithClosingBracket(yamlReader);

				map.put(key, value2);

				continue;
			}

			checkState(!map.containsKey(key), "Duplicate key: " + key);

			final Object value2 = toObject(token2);

			final StringBuilder sb;

			final boolean appendable;

			if (value2 instanceof String) {

				sb = new StringBuilder(value2.toString());

				appendable = true;

			} else {

				sb = null;

				appendable = false;
			}

			Token token3 = null;

			while (true) {

				token3 = yamlReader.readNextToken(false);

				if (token3 == null || token3 == LINEBREAK) {

					break;
				}

				checkState(appendable, "cannot append to value: " + value2);

				final Object value3 = toObject(token3);

				if (value3 instanceof String) {

					sb.append(value3.toString());

				} else {

					throw new NotImplementedException("value3: " + value3);
				}
			}

			map.put(key, appendable ? sb.toString() : value2);

			if (token3 == null) {

				break;

				// return map;
			}

			if (token3 == LINEBREAK) {

				final int indent3 = yamlReader.readIndent();

				if (indent3 == -1) {

					break;
				}

				if (indent3 == indent) {

					continue;
				}

				// final Object value = toObject(token2);

				// map.put(key, value);

				if (indent3 < indent) {

					break;
				}

				continue;
			}

			throw new NotImplementedException("token3: " + token3);
		}

		return map;
	}

	private static Object readArrayWithIndent(
		final YamlReader yamlReader,
		final int indent
	) throws IOException {

		logger.debug("readArrayWithIndent()...");

		final List<Object> list = newArrayList();

		while (true) {

			if (yamlReader.readIndent() < indent) {

				break;
			}

			final Token token0 = yamlReader.readNextToken(false);

			if (!list.isEmpty() && token0 == HYPHEN) {

				continue;
			}

			final int currentIndent = yamlReader.getCurrentTokenColumnNumber() - 2;

			logger.debug("readArrayWithIndent(), token0: " + token0);

			if (token0 == null) {

				throw new YamlReaderException( //
						yamlReader.getCurrentTokenLineNumber(), //
						yamlReader.getCurrentTokenColumnNumber(), //
						"Unterminated hyphen");
			}

			final Token token1 = yamlReader.readNextToken(false);

			if (token1 == null || token1 == LINEBREAK) {

				list.add(toObject(token0));

				final int indent2 = currentIndent;

				if (indent2 < indent) {

					break;
				}

				continue;
			}

			if (token1 == COLON) {

				final Token token2 = yamlReader.readNextToken(false);

				if (token2 == LINEBREAK) {

					final Object value = readValueWithIndent(yamlReader, yamlReader.readIndent());

					final Map<Object, Object> map = newHashMap();

					map.put(toObject(token0), value);

					list.add(map);

					// TODO this works only for one-item lists

					continue;
					// throw new NotImplementedException("");
				}

				final Token token3 = yamlReader.readNextToken(false);

				checkState(token3 == LINEBREAK, "token3 should be LINEBREAK, but was: " + token3);

				final Object value = readValueWithIndent(yamlReader, indent + 2);

				if (List.class.isInstance(value)) {

					@SuppressWarnings("unchecked")
					final List<Object> l = (List<Object>) value;

					list.addAll(l);

				} else if (Map.class.isInstance(value)) {

					@SuppressWarnings("unchecked")
					final Map<Object, Object> map = (Map<Object, Object>) value;

					checkState(!map.containsKey(token0), "Duplicate key: " + token0);

					map.put(toObject(token0), toObject(token2));

					list.add(map);

				} else {

					throw new NotImplementedException("value.class: " + value.getClass().getName());
				}

				continue;
			}

			final Token token2 = yamlReader.readNextToken(false);

			if (token2 != HYPHEN) {

				throw new YamlReaderException( //
						yamlReader.getCurrentTokenLineNumber(), //
						yamlReader.getCurrentTokenColumnNumber(), //
						"Illegal token in array: " + token2);
			}
		}

		return list;
	}
}
