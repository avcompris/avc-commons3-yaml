package net.avcompris.commons3.yaml;

import java.util.Date;

public interface Yaml {

	Iterable<Object> keys();

	/**
	 * Keys are returned sorted.
	 */
	Iterable<String> keysAsStrings();

	Iterable<Yaml> items();

	boolean has(Object key);

	Yaml get(Object key);

	String asString();

	int asInt();

	long asLong();

	boolean asBoolean();

	Date asDate();

	boolean isBoolean();

	boolean isString();

	boolean isInt();

	boolean isArray();

	boolean isMap();
}
