package net.avcompris.commons3.yaml;

import javax.annotation.Nullable;

public class YamlKeyException extends YamlReaderException {

	public YamlKeyException(final int lineNumber, final int columnNumber, @Nullable final String message) {

		super(lineNumber, columnNumber, message);
	}

	/**
	 * For serialization.
	 */
	private static final long serialVersionUID = 6458217980265080681L;

}
