package net.avcompris.commons3.yaml;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class YamlLoaderTest {

	private static Map<Object, Object> load(final String s) throws IOException {

		try (Reader reader = new StringReader(s)) {

			return new YamlLoader().load(reader);
		}
	}

	private static Map<Object, Object> load(final File yamlFile) throws IOException {

		try (InputStream is = new FileInputStream(yamlFile)) {

			try (Reader reader = new InputStreamReader(is, UTF_8)) {

				return new YamlLoader().load(reader);
			}
		}
	}

	@Test
	public void testEmptyMap_brackets() throws Exception {

		final Map<Object, Object> map = load("{}");

		assertEquals(0, map.size());
	}

	@Test
	public void testEmptyMap_no_brackets() throws Exception {

		final Map<Object, Object> map = load("");

		assertEquals(0, map.size());
	}

	@Test
	public void testEmptyMap_space_no_brackets() throws Exception {

		final Map<Object, Object> map = load("  ");

		assertEquals(0, map.size());
	}

	@Test
	public void testEmptyMap_comment() throws Exception {

		final Map<Object, Object> map = load("# toto");

		assertEquals(0, map.size());
	}

	@Test
	public void testEmptyMap_space_comment() throws Exception {

		final Map<Object, Object> map = load("  # toto");

		assertEquals(0, map.size());
	}

	@Test
	public void test_a_3_b_34() throws Exception {

		final Map<Object, Object> map = load("{ a: 3, b: 34 }");

		assertEquals(2, map.size());

		assertEquals(3, map.get("a"));
		assertEquals(34, map.get("b"));
	}

	@Test
	public void testOneItem_brackets_no_spaces() throws Exception {

		assertThrows(RuntimeException.class, () ->

		load("{x:y}"));
	}

	@Test
	public void testOneItem_brackets_with_space() throws Exception {

		final Map<Object, Object> map = load("{x: y }");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_indent() throws Exception {

		final Map<Object, Object> map = load("x:\n" //
				+ "  y");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_indent_error_tab() throws Exception {

		final YamlReaderException e = assertThrows(YamlReaderException.class, () ->

		load("x:\n" //
				+ "\ty"));

		assertEquals(2, e.getLineNumber());
		assertEquals(1, e.getColumnNumber());
	}

	@Test
	public void testOneItem_brackets() throws Exception {

		final Map<Object, Object> map = load("{x: y }");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_brackets_3letters() throws Exception {

		final Map<Object, Object> map = load("{abc: def }");

		assertEquals(1, map.size());

		assertEquals("abc", map.keySet().iterator().next());
		assertEquals("def", map.values().iterator().next());
	}

	@Test
	public void testOneItem_space_in_key() throws Exception {

		final Map<Object, Object> map = load("{ab c: def }");

		assertEquals(1, map.size());

		assertEquals("ab c", map.keySet().iterator().next());
		assertEquals("def", map.values().iterator().next());
	}

	@Test
	public void testOneItem_url() throws Exception {

		final Map<Object, Object> map = load("Apple: https://apple.com");

		assertEquals(1, map.size());

		assertEquals("https://apple.com", map.get("Apple"));
	}

	@Test
	public void testOneItem_brackets_spaces() throws Exception {

		final Map<Object, Object> map = load(" { x : y } ");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_brackets_spaces_quotes() throws Exception {

		final Map<Object, Object> map = load(" { \"x\" : \"y\" } ");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_brackets_quotes() throws Exception {

		final Map<Object, Object> map = load("{\"x\": \"y\"}");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_no_brackets() throws Exception {

		final Map<Object, Object> map = load("x: y");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_no_brackets_quotes() throws Exception {

		final Map<Object, Object> map = load("\"x\": \"y\"");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals("y", map.values().iterator().next());
	}

	@Test
	public void testOneItem_no_brackets_quotes_escaped_quotes() throws Exception {

		final Map<Object, Object> map = load("\"x\\\"a\": \"y\\\"b\"");

		assertEquals(1, map.size());

		assertEquals("x\"a", map.keySet().iterator().next());
		assertEquals("y\"b", map.values().iterator().next());
	}

	@Test
	public void testOneItem_no_brackets_quotes_escaped_backslashes() throws Exception {

		final Map<Object, Object> map = load("\"x\\\\a\": \"y\\\\b\"");

		assertEquals(1, map.size());

		assertEquals("x\\a", map.keySet().iterator().next());
		assertEquals("y\\b", map.values().iterator().next());
	}

	@Test
	public void test_mapAsKey() throws Exception {

		final Map<Object, Object> map = load("{x: y }: z");

		assertEquals(1, map.size());

		assertEquals(1, ((Map<?, ?>) map.keySet().iterator().next()).size());

		assertEquals("x", ((Map<?, ?>) map.keySet().iterator().next()).keySet().iterator().next());
		assertEquals("y", ((Map<?, ?>) map.keySet().iterator().next()).values().iterator().next());

		assertEquals("z", map.values().iterator().next());
	}

	@Test
	public void test_mapAsValue_x_yz_brackets() throws Exception {

		final Map<Object, Object> map = load("x:{y: z }");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());

		assertEquals(1, ((Map<?, ?>) map.values().iterator().next()).size());

		assertEquals("y", ((Map<?, ?>) map.values().iterator().next()).keySet().iterator().next());
		assertEquals("z", ((Map<?, ?>) map.values().iterator().next()).values().iterator().next());
	}

	@Test
	public void test_mapAsValue_x_empty_brackets() throws Exception {

		final Map<Object, Object> map = load("x:{}");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());

		assertEquals(0, ((Map<?, ?>) map.values().iterator().next()).size());
	}

	@Test
	public void test_mapAsValue_indent_x_yz() throws Exception {

		final Map<Object, Object> map = load("x:\n" //
				+ "   y:\n" //
				+ "     z");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());

		assertEquals(1, ((Map<?, ?>) map.values().iterator().next()).size());

		assertEquals("y", ((Map<?, ?>) map.values().iterator().next()).keySet().iterator().next());
		assertEquals("z", ((Map<?, ?>) map.values().iterator().next()).values().iterator().next());
	}

	@Test
	public void test_mapAsValue_indent_x_yz_same_line() throws Exception {

		final Map<Object, Object> map = load("x:\n" //
				+ "   y: z");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());

		assertEquals(1, ((Map<?, ?>) map.values().iterator().next()).size());

		assertEquals("y", ((Map<?, ?>) map.values().iterator().next()).keySet().iterator().next());
		assertEquals("z", ((Map<?, ?>) map.values().iterator().next()).values().iterator().next());
	}

	@Test
	public void test_mapAsValue_indent_x_yz_LF_LF_same_line() throws Exception {

		final Map<Object, Object> map = load("x:\n\n" //
				+ "   y: z");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());

		assertEquals(1, ((Map<?, ?>) map.values().iterator().next()).size());

		assertEquals("y", ((Map<?, ?>) map.values().iterator().next()).keySet().iterator().next());
		assertEquals("z", ((Map<?, ?>) map.values().iterator().next()).values().iterator().next());
	}

	@Test
	public void test_mapAsValue_indent_xy_za() throws Exception {

		final Map<Object, Object> map = load("x:\n" //
				+ "   y\n" //
				+ "z:\n" //
				+ "   a");

		assertEquals(2, map.size());

		assertEquals("y", map.get("x"));
		assertEquals("a", map.get("z"));
	}

	@Test
	public void test_mapAsValue_indent_xy_za_LF() throws Exception {

		final Map<Object, Object> map = load("x:\n" //
				+ "   y\n" //
				+ "z:\n" //
				+ "   a\n");

		assertEquals(2, map.size());

		assertEquals("y", map.get("x"));
		assertEquals("a", map.get("z"));
	}

	@Test
	public void test_mapAsValue_indent_unterminated_colon() throws Exception {

		final YamlReaderException e = assertThrows(YamlReaderException.class, () ->

		load("x:\n" //
				+ "   y:\n" //
				+ "z:\n" //
				+ "   a\n"));

		assertEquals(3, e.getLineNumber());
		assertEquals(1, e.getColumnNumber());
	}

	@Test
	public void test_mapAsValue_indent_x_yz_ab() throws Exception {

		final Map<Object, Object> map = load("x:\n" //
				+ "   y: z\n" //
				+ "   a: b\n");

		assertEquals(1, map.size());

		@SuppressWarnings("unchecked")
		final Map<Object, Object> map2 = (Map<Object, Object>) map.get("x");

		assertEquals("z", map2.get("y"));
		assertEquals("b", map2.get("a"));
	}

	@Test
	public void testInt() throws Exception {

		final Map<Object, Object> map = load("{ x: 124 }");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals(124, map.values().iterator().next());
	}

	@Test
	public void testLong() throws Exception {

		final Map<Object, Object> map = load("{ x: 1240912831237129329 }");

		assertEquals(1, map.size());

		assertEquals("x", map.keySet().iterator().next());
		assertEquals(1240912831237129329L, map.values().iterator().next());
	}

	@Test
	public void testBooleans() throws Exception {

		final Map<Object, Object> map = load("{ x: true, y: false }");

		assertEquals(2, map.size());

		assertEquals(true, map.get("x"));
		assertEquals(false, map.get("y"));
	}

	@Test
	public void test_array_3_34_brackets() throws Exception {

		final Map<Object, Object> map = load("a: [ 3, 34 ]");

		assertEquals(1, map.size());

		@SuppressWarnings("unchecked")
		final List<Object> list = (List<Object>) map.get("a");

		assertEquals(2, list.size());

		assertEquals(3, list.get(0));
		assertEquals(34, list.get(1));
	}

	@Test
	public void test_value_with_processing_variable() throws Exception {

		final Map<Object, Object> map = load("get: ${baseUrl}");

		assertEquals(1, map.size());

		assertEquals("${baseUrl}", map.get("get"));
	}

	@Test
	public void test_array_3_34_brackets_no_spaces() throws Exception {

		final Map<Object, Object> map = load("a: [3, 34 ]");

		assertEquals(1, map.size());

		@SuppressWarnings("unchecked")
		final List<Object> list = (List<Object>) map.get("a");

		assertEquals(2, list.size());

		assertEquals(3, list.get(0));
		assertEquals(34, list.get(1));
	}

	@Test
	public void test_array_indent() throws Exception {

		final Map<Object, Object> map = load("a:\n" //
				+ "    - 3\n" //
				+ "    - 34");

		assertEquals(1, map.size());

		@SuppressWarnings("unchecked")
		final List<Object> list = (List<Object>) map.get("a");

		assertEquals(2, list.size());

		assertEquals(3, list.get(0));
		assertEquals(34, list.get(1));
	}

	@Test
	public void test_array_empty_brackets() throws Exception {

		final Map<Object, Object> map = load("a: []");

		assertEquals(1, map.size());

		@SuppressWarnings("unchecked")
		final List<Object> list = (List<Object>) map.get("a");

		assertEquals(0, list.size());
	}

	@Test
	public void test_quoted_values_with_single_quotes() throws Exception {

		final Map<Object, Object> map = load("click: \"div[@id = '#div-sidebar'] a[@href = '/uploads']\"");

		assertEquals("div[@id = '#div-sidebar'] a[@href = '/uploads']", map.get("click"));
	}

	@Test
	public void test_quoted_values_with_single_quotes_two_lines() throws Exception {

		final Map<Object, Object> map = load("click: \"div[@id = '#div-sidebar'] a[@href = '/uploads']\"\n" //
				+ "bla: blah");

		assertEquals("div[@id = '#div-sidebar'] a[@href = '/uploads']", map.get("click"));
		assertEquals("blah", map.get("bla"));
	}

	@Test
	public void test_unquoted_values_with_hash() throws Exception {

		final Map<Object, Object> map = load("click: div[@id = '#div-sidebar'] a[@href = '/uploads']");

		assertEquals("div[@id = '#div-sidebar'] a[@href = '/uploads']", map.get("click"));
	}

	@Test
	public void test_quoted_values_with_single_quotes_two_lines_in_array() throws Exception {

		final Map<Object, Object> map = load("a:\n" //
				+ "  - x: y\n"//
				+ "    click: \"div[@id = '#div-sidebar'] a[@href = '/uploads']\"\n" //
				+ "    bla: blah");

		final List<?> list = (List<?>) map.get("a");

		final Map<?, ?> map2 = (Map<?, ?>) list.iterator().next();

		assertEquals("div[@id = '#div-sidebar'] a[@href = '/uploads']", map2.get("click"));
		assertEquals("blah", map2.get("bla"));
	}

	@Test
	public void test_array_brackets_illegal_comma() throws Exception {

		assertThrows(YamlReaderException.class, () ->

		load("a: [,]"));
	}

	@Test
	public void test_array_with_comments() throws Exception {

		final Map<Object, Object> map = load(new File("src/test/yaml", "0004.yaml"));

		assertEquals(1, map.size());

		@SuppressWarnings("unchecked")
		final List<Object> list = (List<Object>) map.values().iterator().next();

		assertEquals(17, list.size());

		assertEquals("101110", list.get(0));
		assertEquals("10111", list.get(1));
	}

	@Test
	public void test_space_in_value() throws Exception {

		// tests:
		// healthcheck:
		// authentified: false
		// request: GET /healthcheck
		// response: 200
		//
		final Map<Object, Object> map = load(new File("src/test/yaml", "api-tests_001.yml"));

		assertEquals(1, map.size());

		assertEquals("tests", map.keySet().iterator().next());

		final Map<?, ?> subMap = (Map<?, ?>) map.values().iterator().next();

		assertEquals(1, subMap.size());

		assertEquals("healthcheck", subMap.keySet().iterator().next());

		final Map<?, ?> subMap2 = (Map<?, ?>) subMap.values().iterator().next();

		assertEquals(3, subMap2.size());

		assertEquals(false, subMap2.get("authentified"));
		assertEquals("GET /healthcheck", subMap2.get("request"));
		assertEquals(200, subMap2.get("response"));
	}

	@Test
	public void test_regular_config_002() throws Exception {

		final Map<Object, Object> map = load(new File("src/test/yaml", "api-tests_002.yml"));

		assertEquals(1, map.size());

		final Map<?, ?> subMap = (Map<?, ?>) map.values().iterator().next();

		assertEquals(6, subMap.size());

		final Map<?, ?> create_user_get_users = (Map<?, ?>) subMap.get("create_user_get_users");

		assertEquals(4, ((List<?>) create_user_get_users.get("steps")).size());
	}

	@Test
	public void test_regular_config_002_simplified() throws Exception {

		final Map<Object, Object> map = load(new File("src/test/yaml", "api-tests_002_simplified.yml"));

		assertEquals(1, map.size());

		assertEquals("tests", map.keySet().iterator().next());

		final Map<?, ?> subMap = (Map<?, ?>) map.values().iterator().next();

		assertEquals(1, subMap.size());

		final Map<?, ?> create_user_get_users = (Map<?, ?>) subMap.get("create_user_get_users");

		final List<?> list = (List<?>) create_user_get_users.get("steps");

		assertEquals(4, list.size());

		// request: GET /api/v1/users
		// response:
		// status: 200
		// class: DummyUsersInfo
		// let:
		// count0: response.data.total
		//
		final Map<?, ?> item0 = (Map<?, ?>) list.get(0);

		assertEquals(3, item0.size());

		assertEquals("GET /api/v1/users", item0.get("request"));
		assertEquals(2, ((Map<?, ?>) item0.get("response")).size());
		assertEquals(200, ((Map<?, ?>) item0.get("response")).get("status"));
		assertEquals("DummyUsersInfo", ((Map<?, ?>) item0.get("response")).get("class"));
		assertEquals(1, ((Map<?, ?>) item0.get("let")).size());
		assertEquals("response.data.total", ((Map<?, ?>) item0.get("let")).get("count0"));
	}
}
