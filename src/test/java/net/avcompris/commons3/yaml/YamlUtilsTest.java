package net.avcompris.commons3.yaml;

import static com.google.common.collect.Iterables.getOnlyElement;
import static com.google.common.collect.Iterables.size;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.util.Iterator;

import org.junit.jupiter.api.Test;

public class YamlUtilsTest {

	@Test
	public void testLoadYaml_repos_001() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml(new File("src/test/yaml", "repos-001.yaml"));

		assertEquals(1, size(yaml.keys()));
		assertEquals(1, size(yaml.keysAsStrings()));

		assertEquals("myrepo-1", getOnlyElement(yaml.keys()));
		assertEquals("myrepo-1", getOnlyElement(yaml.keysAsStrings()));

		final Yaml repoYaml = yaml.get(getOnlyElement(yaml.keys()));

		assertEquals(5, size(repoYaml.keys()));
		assertEquals(5, size(repoYaml.keysAsStrings()));
		assertEquals("My Repo #1", repoYaml.get("displayName").asString());
		assertEquals("http://xxx", repoYaml.get("nodeUrl").asString());
		assertEquals(true, repoYaml.get("enabled").asBoolean());
		assertEquals(1, repoYaml.get("implementationVersion").asInt());
		assertEquals(6, size(repoYaml.get("abilities").keys()));
		assertEquals(6, size(repoYaml.get("abilities").keysAsStrings()));
		assertEquals(true, repoYaml.get("abilities").get("parentTrace").asBoolean());
		assertEquals(true, repoYaml.get("abilities").get("containerTrace").asBoolean());
		assertEquals(true, repoYaml.get("abilities").get("closureTraces").asBoolean());
		assertEquals(false, repoYaml.get("abilities").get("multiOwner").asBoolean());
		assertEquals(false, repoYaml.get("abilities").get("multiPoster").asBoolean());
		assertEquals(false, repoYaml.get("abilities").get("traceViews").asBoolean());
	}

	@Test
	public void testLoadYaml_sorted_already() throws Exception {

		final Yaml yaml1 = YamlUtils.loadYaml("{ a: 3, b: 3 }");

		assertEquals(2, size(yaml1.keys()));
		assertEquals(2, size(yaml1.keysAsStrings()));

		int i;

		i = 0;

		for (final String key : yaml1.keysAsStrings()) {

			assertEquals(new String[] { "a", "b" }[i], key, "#" + i);

			++i;
		}
	}

	@Test
	public void testLoadYaml_sorted_reversed() throws Exception {

		final Yaml yaml1 = YamlUtils.loadYaml("{ b: 3, a: 3 }");

		assertEquals(2, size(yaml1.keys()));
		assertEquals(2, size(yaml1.keysAsStrings()));

		int i;

		i = 0;

		for (final String key : yaml1.keysAsStrings()) {

			assertEquals(new String[] { "a", "b" }[i], key, "#" + i);

			++i;
		}
	}

	@Test
	public void testLoadYaml_doubleQuoted_value() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("{ b: \"abc\" }");

		assertEquals("abc", yaml.get("b").asString());
	}

	@Test
	public void testLoadYaml_singleQuoted_value() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("{ b: 'abc' }");

		assertEquals("abc", yaml.get("b").asString());
	}

	@Test
	public void testLoadYaml_singleQuoted_value_plus_trailing_text() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("b: 'abc' xxx");

		assertEquals("'abc' xxx", yaml.get("b").asString());
	}

	@Test
	public void testLoadYaml_singleQuoted_value_plus_trailing_text_and_spaces() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("b: 'abc' xxx   ");

		assertEquals("'abc' xxx", yaml.get("b").asString());
	}

	@Test
	public void testLoadYaml_singleQuote_in_value() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("b: a'bc");

		assertEquals("a'bc", yaml.get("b").asString());
	}

	@Test
	public void testLong() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("{ x: 1240912831237129329 }");

		assertEquals(1240912831237129329L, yaml.get("x").asLong());
		assertEquals("1240912831237129329", yaml.get("x").asString());
	}

	@Test
	public void testInt() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("{ x: 124 }");

		assertEquals(124, yaml.get("x").asInt());
		assertEquals(124, yaml.get("x").asLong());
		assertEquals("124", yaml.get("x").asString());
	}

	@Test
	public void testLoadYaml_asBoolean_asString() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("{ x: true, y: false }");

		assertEquals(2, size(yaml.keys()));

		assertEquals(true, yaml.get("x").asBoolean());
		assertEquals(false, yaml.get("y").asBoolean());

		assertEquals("true", yaml.get("x").asString());
		assertEquals("false", yaml.get("y").asString());
	}

	@Test
	public void testLoadYaml_1024() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml(new File("src/test/yaml", "1024.yaml"));

		final String key = "1010001011101010110111000000110110010010111000111100010100010100011000001111000100111111001101100000001101010110110000111111000001110000110011001101101000001001101110110001000011011011101000011110000001100111100000010001110100010110100111101101111010011101101000101110101011011100000011011001001011100011110001010001010001100000111100010011111100110110000000110101011011000011111100000111000011001100110110100000100110111011000100001101101110100001111000000110011110000001000111010001011010011110110111101001110110100010111010101101110000001101100100101110001111000101000101000110000011110001001111110011011000000011010101101100001111110000011100001100110011011010000010011011101100010000110110111010000111100000011001111000000100011101000101101001111011011110100111011010001011101010110111000000110110010010111000111100010100010100011000001111000100111111001101100000001101010110110000111111000001110000110011001101101000001001101110110001000011011011101000011110000001100111100000010001110100010110100111101101111010011101";

		assertEquals(key, yaml.keys().iterator().next().toString());

		assertEquals( //
				"11110100011000000100101000010100010111000101010110100111100111101001000101101001110111101101000100000101000000100010010111101000101010010011001101000111000011101001100010011001010010010111001011010000100110110100000110101011101000011110111001001101111011000111010001100000010010100001010001011100010101011010011110011110100100010110100111011110110100010000010100000010001001011110100010101001001100110100011100001110100110001001100101001001011100101101000010011011010000011010101110100001111011100100110111101100011101000110000001001010000101000101110001010101101001111001111010010001011010011101111011010001000001010000001000100101111010001010100100110011010001110000111010011000100110010100100101110010110100001001101101000001101010111010000111101110010011011110110001110100011000000100101000010100010111000101010110100111100111101001000101101001110111101101000100000101000000100010010111101000101010010011001101000111000011101001100010011001010010010111001011010000100110110100000110101011101000011110111001001101111011000",
				yaml.get(key).items().iterator().next().asString());
	}

	@Test
	public void test_array_with_spaces() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("c:\n  - a b\n  - xx yy").get("c");

		assertTrue(yaml.isArray());

		final Iterator<Yaml> it = yaml.items().iterator();

		assertEquals("a b", it.next().asString());
		assertEquals("xx yy", it.next().asString());

		assertFalse(it.hasNext());
	}

	@Test
	public void test_array_with_hyphens() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("c:\n" + //
				"  - a -b\n" + //
				"  - xx -yy").get("c");

		assertTrue(yaml.isArray());

		final Iterator<Yaml> it = yaml.items().iterator();

		assertEquals("a -b", it.next().asString());
		assertEquals("xx -yy", it.next().asString());

		assertFalse(it.hasNext());
	}

	@Test
	public void test_array_with_brackets() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("monitoring:\n" + //
				"  - type: munin\n" + //
				"    node_name: ks11-nginx.avcompris.com\n" + //
				"    graphs: [nginx_request, nginx–status ]").get("monitoring");

		assertTrue(yaml.isArray());

		final Yaml monitoring = yaml.items().iterator().next();

		assertTrue(monitoring.isMap());

		assertEquals("munin", monitoring.get("type").asString());
		assertEquals("ks11-nginx.avcompris.com", monitoring.get("node_name").asString());

		final Yaml graphs = monitoring.get("graphs");

		assertTrue(graphs.isArray());

		final Iterator<Yaml> it = graphs.items().iterator();
		assertTrue(it.next().isString());
		assertTrue(it.next().isString());
		assertFalse(it.hasNext());
	}

	@Test
	public void test_array_indents() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("c:\n" + //
				"  xxx:\n" + //
				"    - aaa: bbb\n" + //
				"  yyy: zzz").get("c");

		assertTrue(yaml.isMap());

		assertTrue(yaml.get("xxx").isArray());
		assertTrue(yaml.get("xxx").items().iterator().next().isMap());
		assertEquals("bbb", yaml.get("xxx").items().iterator().next().get("aaa").asString());

		assertEquals("zzz", yaml.get("yyy").asString());
	}

	@Test
	public void test_map_value_with_comma() throws Exception {

		final Yaml yaml = YamlUtils.loadYaml("x: a, b, c").get("x");

		assertTrue(yaml.isString());

		assertEquals("a, b, c", yaml.asString());
	}
}
